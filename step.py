import os
import requests
import hashlib


def get_git_revision_hash() -> str:
    return os.environ['commit_hash']


try:
    build_status = os.environ['build_status']
    if build_status == 'AUTO':
        bitrise_build_status = os.getenv('BITRISE_BUILD_STATUS')
        if bitrise_build_status == "0":
            build_status = 'SUCCESSFUL'
        else:
            build_status = 'FAILED'

    print('Setting bitbucket build status too: {}'.format(build_status))
    key = hashlib.md5(
        ('{}-{}'.format(os.environ['build_key_prefix'], os.getenv('BITRISE_BUILD_SLUG'))).encode()).hexdigest()
    data = {
        'key': key,
        'state': build_status,
        'name': 'Bitrise build #{} - {}'.format(os.getenv('BITRISE_BUILD_NUMBER'), os.getenv('BITRISE_TRIGGERED_WORKFLOW_ID')),
        'url': os.getenv('BITRISE_BUILD_URL'),
        'description': 'Workflow: {}'.format(os.getenv('BITRISE_TRIGGERED_WORKFLOW_ID'))
    }

    api_url = ('https://api.bitbucket.org/2.0/repositories/'
               '%(owner)s/%(repo_slug)s/commit/%(revision)s/statuses/build'
               % {'owner': os.environ['bitbucket_account_name'],
                  'repo_slug': os.getenv('BITRISEIO_GIT_REPOSITORY_SLUG'),
                  'revision': get_git_revision_hash()})

    r = requests.post(api_url, auth=(
        os.environ['bitbucket_username'], os.environ['bitbucket_password']), json=data)
    if not r.ok:
        print(r.request.url)
        print(data)
        print(r.content)

except:
    print("Error while sending bitbucket status")
