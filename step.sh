#!/bin/bash
set -e

#!/bin/bash

THIS_SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Set up a virtual environment
python3 -m venv "$THIS_SCRIPT_DIR/venv"
source "$THIS_SCRIPT_DIR/venv/bin/activate"

pip3 install requests

echo '$' "python "$THIS_SCRIPT_DIR/step.py""
python3 "$THIS_SCRIPT_DIR/step.py"

deactivate
